<?php
use Application\Controller\Rest\ChatController;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/[:action]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
        	'rest' => array(
        		'type' => 'Literal',
        		'options' => array(
        			'route' => '/rest',
        			'defaults' => array(
        				'__NAMESPACE__' => '',
        				'controller' => 'chat',
        			),
        		),
        		'may_terminate' => true,
        		'child_routes' => array(
        			'restful' => array(
						'type' => 'Segment',
		        		'options' => array(
		        			'route' => '/:controller[/:id]',
		        		),
        			),
        			'custom' => array(
        				'type' => 'Segment',
        				'options' => array(
        					'route' => '/:controller/:action/[:arg]'
        				),
        			),
        		),
        	),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => array('cz', 'en'),
        'translation_file_patterns' => array(
            array(
                'type' => 'phparray',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.php',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'chat' => 'Application\Controller\Rest\ChatController',
            'resources' => 'Application\Controller\Rest\ResourcesController',
//             		=> function() {
//             	return new ChatController();
//     		},
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/plain'            => __DIR__ . '/../view/layout/plain.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
        	'ViewJsonStrategy',
        ),
    ),
);
