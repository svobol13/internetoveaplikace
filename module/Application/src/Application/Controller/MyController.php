<?php
namespace Application\Controller;

use Zend\View\Helper\ViewModel;

use Zend\Mvc\Controller\AbstractActionController;

class MyController extends AbstractActionController {
	
	public function indexAction() {
		return new ViewModel();	
	}
	
}