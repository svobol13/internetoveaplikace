<?php

namespace Application\Controller\Rest;

use Application\Db\MessageTable;

use Zend\View\Model\JsonModel;

use Zend\Mvc\Controller\AbstractRestfulController;

class ChatController extends AbstractRestfulController {

	/**
	 * @var MessageTable
	 */
	private $messagesTable;


	public function get($id) {
		$data = array('jmeno' => 'franta', 'id' => $id);
		return new JsonModel(array("data" => $data));
	}

	public function delete($id) {
        echo 'delete';
	}

	public function update($id, $data) {
        echo 'update';
	}

	public function create($data) {
		$this->getMessagesTable()->sendMessage($data['sender'], $data['message'], $data['time']);
        return new JsonModel();
	}

	public function getList() {
        echo "get list";
	}

    public function rest() {
        return $this->get("rest test");
    }

	public function messagesAction() {
        $before = microtime();
		$json = new JsonModel($this->getMessagesTable()->getAllMessages());
        $takes = microtime() - $before;
        return $json;
// 		return new JsonModel(array(array("sender" => "Pepa", "message" => "zprava", "time" => "2013-04-16 14:55:50")));
	}

	/**
	 * @return \Application\Db\MessageTable
	 */
	private function getMessagesTable() {
		if (!$this->messagesTable) {
			$this->messagesTable = $this->getServiceLocator()->get("messagesTable");
		}
		return $this->messagesTable;
	}

}