<?php

namespace Application\Controller\Rest;

use Application\Db\PageTable;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;

class ResourcesController extends AbstractRestfulController {

    /**
     * @var PageTable
     */
    private $pageTable;
    private $set = "settings";
    /**
     * @var type Container
     */
    private $session;

    public function __construct() {
        $this->session = new Container('base');
    }
    
    public function get($id) {
        if ($id === $this->set) {
            if (!$this->session->offsetExists($this->set)) {
                return new JsonModel();
            }
            $sett = $this->session->offsetGet($this->set);
            return new JsonModel($sett);
        }
        return new JsonModel($this->getPageTable()->getPage($id));
    }

    public function delete($id) {
        echo 'delete';
    }

    public function update($id, $data) {
        if ($id === $this->set) {
            // $_SESSION[$this->set] = $data;
            $this->session->offsetSet($this->set, $data);
        }
        return new JsonModel();
    }

    public function create($data) {
        $this->getMessagesTable()->sendMessage($data['sender'], $data['message'], $data['time']);
        return new JsonModel();
    }

    public function getList() {
        echo "get list";
    }

    public function rest() {
        return $this->get("rest test");
    }

    public function pagesAction() {
        return new JsonModel($this->getPageTable()->getAllPageNames());
    }

    public function userSettingAction() {
        
    }

    /**
     * @return PageTable
     */
    private function getPageTable() {
        if (!$this->pageTable) {
            $this->pageTable = $this->getServiceLocator()->get("pageTable");
        }
        return $this->pageTable;
    }

}