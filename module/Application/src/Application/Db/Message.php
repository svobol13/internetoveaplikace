<?php
namespace Application\Db;

use Common\Mo\BaseModelObject;

final class Message extends BaseModelObject {

	/** @var string */
	public $sender;
	/** @var DateTime */
	public $time;
	/** @var string */
	public $message;

}