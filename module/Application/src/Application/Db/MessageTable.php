<?php
namespace Application\Db;

use Zend\Db\Sql\Insert;

use Zend\Db\Sql\Select;

use Common\Table\BaseTable;

final class MessageTable extends BaseTable {

    /**
     * @return array $messages
     */
    public function getAllMessages() {
        $select = new Select($this->table);
        $select->order('time DESC');
        $select->limit(5);
        $result = $this->selectWith($select);
        $result = array_reverse($result->toArray());
        return $result;
    }


    public function sendMessage($sender, $message, $time) {
        $insert = new Insert();
        $insert->into($this->table);
        $insert->values(array('sender' => $sender, 'message' => $message, 'time' => $time));
        $result = $this->insertWith($insert);
    }

}