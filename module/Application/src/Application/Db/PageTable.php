<?php
namespace Application\Db;

use Zend\Db\Sql\Predicate\Predicate;

use Zend\Db\Sql\Where;

use Zend\Db\Sql\Select;

use Common\Table\BaseTable;

final class PageTable extends BaseTable {

	/**
	 * @return array $names Array of string names.
	 */
	public function getAllPageNames() {
		$select = new Select($this->table);
		$select->columns(array('name'));
		$result = $this->selectWith($select);
		$new = array();
        foreach ($result->toArray() as $page) {
            $new[] = $page['name'];
        }
        return $new;
	}

	/**
	 * @param string $name
	 * @return string $content
	 */
	public function getPage($name) {
		$select = new Select($this->table);
		$predicate = new Where();
		$predicate->equalTo("name", $name);
		$select->where($predicate);
		$result = $this->selectWith($select);
		$result = $result->toArray();
        return array($result[0]['content']);
	}

}

