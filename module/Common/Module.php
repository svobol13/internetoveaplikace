<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Common;

use Zend\ModuleManager\Feature\ServiceProviderInterface;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Zend\ModuleManager\Feature\BootstrapListenerInterface;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;

use Common\Service\MediaLibrarySerivce;

use Zend\Stdlib\Parameters;

use Zend\Http\PhpEnvironment\Request;

use Zend\EventManager\EventManager;

use Common\Service\ManagementService;

use Common\Service\BookingService;

use Common\Service\CrmService;

use Zend\EventManager\EventManagerInterface;

use Common\Utils\LocaleUtils;

use Common\Service\BaseService;

use Common\Security\Guard\ServiceGuard;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements
AutoloaderProviderInterface,
ConfigProviderInterface,
ServiceProviderInterface {
    /**
     * @var EventManager
     */
    private $em;

    public function onBootstrap(MvcEvent $e) {
    }

    public function getServiceConfig() {
        $em;
        return array(
            'factories' => array(

                //~ SERVICE FACTORIES
//                 'Common\Security\Guard\ServiceGuard' => function ($sm) {
//                     $serviceGuard = new ServiceGuard($sm);
//                     return $serviceGuard;
//                 },

            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
