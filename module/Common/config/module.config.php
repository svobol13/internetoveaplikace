<?php

use Application\Db\PageTable;

use Application\Db\MessageTable;

use Application\Db\SettingsTable;

use Application\Utils\Configuration;

use Application\Db\ShowTable;

use Common\Hydrator\ModelObjectHydrator;

use Common\Validator\Validators;

use Common\Utils\LocaleUtils;

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonWeb for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
//~ DATABASE SETTINGS
$dbParams = array(
	'database'  => 'internetoveaplikace',
	'username'  => 'root',
	'password'  => '',
	'hostname'  => 'localhost',
);
// dsn's
$dsnMysql = 'mysql:dbname='.$dbParams['database'].';host='.$dbParams['hostname'].';charset=utf-8';

return array(
	'service_manager' => array(
		'factories' => array(
			'Zend\Db\Adapter\Adapter' => function ($sm) use ($dbParams, $dsnMysql) {
				$adapter = new Zend\Db\Adapter\Adapter(array(
					'driver'    => 'pdo',
					'dsn'       => ($dsnMysql),
					'database'  => $dbParams['database'],
					'username'  => $dbParams['username'],
					'password'  => $dbParams['password'],
					'hostname'  => $dbParams['hostname'],
				    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                    PDO::ATTR_PERSISTENT => true,
				));
				return $adapter;
			},
			'db_adapter' => function ($sm) {
				return $sm->get('Zend\Db\Adapter\Adapter');
			},


			//~ Table factories

			'messagesTable' => function ($sm) {
				return new MessageTable($sm, 'messages');
			},

			'pageTable' => function ($sm) {
			    return new PageTable($sm, 'pages');
			},

			//~ Utils

			'Cong' => function ($sm) {
                return new Configuration($sm);
			},

			//~ Hydrator initializers

			'ObjectPropertyHydrator' => function ($sm) {
				return new ObjectProperty();
			},
			'ModelObjectHydrator' => function ($sm) {
				return new ModelObjectHydrator();
			},


			//~ Validator factories

			'Validators' => function ($sm) {
				return new Validators($sm);
			},
		),
	),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/public'           => __DIR__ . '/../view/layout/layout.phtml',
            'common/user/index' => __DIR__ . '/../view/common/user/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            'common' => __DIR__ . '/../view',
        ),
    ),
);
