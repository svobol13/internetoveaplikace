<?php
namespace Common\Controller;

use Common\Service\MediaLibrarySerivce;

use Common\Validator\Validators;

use Common\Service\ManagementService;

use ZfcUser\Service\User;

use Common\Mo\UserMo;

use Common\Service\BookingService;

use Common\Service\CrmService;

use Zend\Mvc\Controller\AbstractActionController;

/**
 *
 * @author Lukas Svoboda
 */
class BaseController extends AbstractActionController {

	/**
	 * Returns whole UserMo object of currently authenticated user.
	 *
	 * @return UserMo
	 */
	protected function getAuthenticatedUser() {
		// TODO nahovno zpusob to tahat dvakrat
		$table = $this->getServiceLocator()->get('UserTable');
		$user = $this->getCrmService()->findUserById($this->getAuthenticatedUserId());
        return $user;
	}

	/**
	 * Returns id of authenticated user.
	 *
	 * @return int
	 */
	protected function getAuthenticatedUserId() {
		return $this->zfcUserAuthentication()->getIdentity()->getId();
	}

	/**
	 * @return User
	 */
	protected function getZfcUserService() {
		return $this->getServiceLocator()->get('zfcuser_user_service');
	}

	/**
	 * @return Validators
	 */
	protected function getValidators() {
		return $this->getServiceLocator()->get('Validators');
	}

	protected function getQueryParam($paramName) {
		$request = $this->getEvent()->getParam('request');
		if (isset($request)) {
			return $request->getQuery('id');
		}
		return null;
	}
}