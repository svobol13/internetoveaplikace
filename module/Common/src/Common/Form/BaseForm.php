<?php
namespace Common\Form;

use Zend\Form\Form;
use \Common\Hydrator\FormHydrator;

class BaseForm extends Form {

    /**
     * Get the hydrator used when binding an object to the fieldset
     *
     * @return HydratorInterface
     */
    public function getHydrator() {
        if($this->hydrator == null){
           $this->setHydrator(new FormHydrator());
        }
        return $this->hydrator;
    }
}
