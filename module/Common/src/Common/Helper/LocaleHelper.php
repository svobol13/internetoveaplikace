<?php

namespace Common\Helper;

use Common\Utils\LocaleUtils;

use Zend\View\Helper\AbstractHelper;

class LocaleHelper extends AbstractHelper {
    
	/**
	 * __invoke
	 * @access public
	 * @return String
	 */
	public function __invoke() {
		return LocaleUtils::getLocale();
	}
	
}