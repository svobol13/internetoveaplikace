<?php

namespace Common\Hydrator;

use Exception;
use Zend\Stdlib\Hydrator\AbstractHydrator;
use Zend\XmlRpc\Server\Exception\BadMethodCallException;

/**
 * Hydrator used especialy for ModelObject hydration from array,
 * where attributes in array are CamelCased as in ModelObject (f.e. from form)
 * 
 */
final class FormHydrator extends AbstractHydrator {

    /**
     * Extract values from an object
     *
     * Extracts the accessible non-static properties of the given $object.
     *
     * @param  object $object
     * @return array Underscore keyed array.
     * @throws BadMethodCallException for a non-object $object
     */
    public function extract($object) {
        if (!is_object($object)) {
            return;
            throw new Exception(sprintf(
                            '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }

        $data = get_object_vars($object);
        $extract = array();
        foreach ($data as $key => $value) {
            $extract[$key] = $this->extractValue($key, $value);
        }

        return $extract;
    }

    /**
     * Hydrate an object by populating public properties
     * Hydrates an object by setting public properties of the object.
     *
     * @param  array $data Underscore case keyed array.
     * @param  object $object
     * @return object Camelcased object.
     * @throws BadMethodCallException for a non-object $object
     */
    
    public function hydrate(array $data, $object) {
        if (!is_object($object)) {
            throw new BadMethodCallException(sprintf(
                            '%s expects the provided $object to be a PHP object)', __METHOD__
            ));
        }
        
        foreach($object as $attribute => $value){
            $object->$attribute = (isset($data[$attribute]) && $data[$attribute] != null) ? $data[$attribute] : null;
        }

        return $object;
    }
    
}