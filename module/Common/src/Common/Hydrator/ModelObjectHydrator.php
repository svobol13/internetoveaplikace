<?php
namespace Common\Hydrator;

use Zend\Db\ResultSet\ResultSetInterface;

use Common\Mo\BaseModelObject;

use Common\Utils\StringUtils;

use Zend\Stdlib\Hydrator\AbstractHydrator;

/**
 * Hydrator used especialy for object hydration and data extraction
 * between data tier and service tier.
 *
 * @author Lukas Svoboda
 */
final class ModelObjectHydrator extends AbstractHydrator {

	private $skipNulls = true;

	/**
	 * Extract values from an object
	 *
	 * Extracts the accessible non-static properties of the given $object.
	 *
	 * @param  object $object
	 * @return array Underscore keyed array.
	 * @throws Exception\BadMethodCallException for a non-object $object
	 */
	public function extract($object)
	{
		if (!is_object($object)) {
			return;
			throw new \Exception(sprintf(
				'%s expects the provided $object to be a PHP object)', __METHOD__
			));
		}

		$self = $this;
		$data = get_object_vars($object);
		$extract = array();
		foreach ($data as $key => $value) {
			if ($value == null) {
				continue;
			}
			$extract[StringUtils::camelCaseToUnderscoreCase($key)]
				= $this->extractValue($key, $value);
		}

		return $extract;
	}

	/**
	 * Hydrate an object by populating public properties
	 *
	 * Hydrates an object by setting public properties of the object.
	 *
	 * @param  array $data Underscore case keyed array.
	 * @param  object $object
	 * @return object Camelcased object.
	 * @throws Exception\BadMethodCallException for a non-object $object
	 */
	public function hydrate(array $data, $object)
	{
		if (!is_object($object)) {
			throw new Exception\BadMethodCallException(sprintf(
				'%s expects the provided $object to be a PHP object)', __METHOD__
			));
		}
		foreach ($data as $property => $value) {
			$property = StringUtils::underscoreCaseToCamelCase($property);
			$object->$property = $this->hydrateValue($property, $value);
		}

		return $object;
	}

	/**
	 * Takes the given result set and prototype model object and returns
	 * array of prototype objects.
	 *
	 * @param ResultSetInterface $resultSet
	 * @param BaseModelObject $prototype
	 * @return array $result;
	 */
	public function asArray(ResultSetInterface $resultSet, BaseModelObject $prototype) {
		$array = array();
		foreach ($resultSet as $item) {
			$array[] = $this->hydrate($item->getArrayCopy(), clone $prototype);
		}
		return $array;
	}
}