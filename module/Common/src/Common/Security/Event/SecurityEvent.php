<?php
namespace Common\Security\Event;

use Zend\EventManager\Event;

/**
 * Event  
 * @author Lukas Svoboda
 */
class SecurityEvent extends Event {
	
	const EVENT_ACCESS = 'access';
	
}