<?php
namespace Common\Security\Guard;

use BjyAuthorize\Service\Authorize;

use Zend\ServiceManager\ServiceManager;

use Zend\EventManager\Event;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\EventManager\StaticEventManager;
use Zend\EventManager\SharedEventManager;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\Stdlib\CallbackHandler;
use Zend\ServiceManager\ServiceLocatorInterface;

use Common\Security\Event\SecurityEvent;

use BjyAuthorize\Provider\Rule\ProviderInterface as Rule;
use BjyAuthorize\Provider\Resource\ProviderInterface as Resource;
use BjyAuthorize\Guard\GuardInterface;

/**
 * @author Lukas Svoboda
 */
class ServiceGuard implements GuardInterface, Resource, Rule, ServiceLocatorAwareInterface {

	/**
	 * @var ServiceLocatorInterface
	 */
	protected $serviceLocator;
	/**
	 * @var Authorize
	 */
	protected $securityService;
	/**
	 * @var CallbackHandler
	 */
	protected $listeners = array();

	protected $rules = array();

	public function __construct(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
		$this->securityService = $serviceLocator->get('BjyAuthorize\Service\Authorize');
	}

	public function attach(EventManagerInterface $events) {
		$this->listeners[] = $events->attach(SecurityEvent::EVENT_ACCESS, array($this, 'onAccess'), -1000);
	}

	public function detach(EventManagerInterface $events) {
		foreach ($this->listeners as $index => $listener) {
			if ($events->detach($listener)) {
				unset($this->listeners[$index]);
			}
		}
	}

	public function getResources() {
		$resources = array();
		foreach ($this->rules as $resource => $roles) {
			$resources[] = $resource;
		}
		return $resources;
	}

	public function getRules() {
		$rules = array();
		foreach ($this->rules as $resource => $roles) {
			$rules[] = array($roles, $resource);
		}
		return array('allow' => $rules);
	}

	public function onAccess(Event $e) {
		$args = $e->getParams();
		$class = $args['class'];
		$method = $args['method'];

		$class = sprintf('service/%s', $class);
		$method = sprintf('%s:%s', $class, $method);
		$allowed = $this->securityService->isAllowed($class) || $this->securityService->isAllowed($method);
		if (!$allowed) {
			throw new \Exception(sprintf('access is dennied! class: %s, method: %s', $class, $method));
		}
	}

	public static function getResourceName($class, $method) {
		if (!isset($method)) {
			return sprintf('service/%s', $class);
		} else {
			return sprintf('service/%s:%s', $class, $method);
		}
	}

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}
	public function getServiceLocator() {
		return $this->serviceLocator;
	}

}