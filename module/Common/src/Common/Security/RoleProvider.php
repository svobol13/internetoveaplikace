<?php
namespace Common\Security;

use BjyAuthorize\Acl\Role;

use BjyAuthorize\Provider\Role\ProviderInterface;

class RoleProvider implements ProviderInterface {

	private $roles = array();

	public function __construct($options, $serviceManager) {
		$roles = array();
		foreach ($options as $roleId => $roleOptions) {
			$roles[] = new Role($roleId, $roleOptions['parent']);
		}
		$this->roles = $roles;
	}

	public function getRoles() {
		return $this->roles;
	}

}