<?php
namespace Common\Security;

/**
 * Rule matrix implementation.
 *
 * @author Lukas Svoboda
 */
class RuleMatrix implements RuleMatrixInterface {

	private $rules;
	private $resources;
	private $roles;

	public function __construct($rules, $resources, $roles) {
		$this->rules = $rules;
		$this->resources = $resources;
		$this->roles = $roles;
	}

	public function getRoles() {
		return $this->roles;
	}

	public function getResources() {
		return $this->resources;
	}

	public function isAllowed($roleId, $resourceName) {
		return isset($this->rules[$resourceName][$roleId]);
	}
}