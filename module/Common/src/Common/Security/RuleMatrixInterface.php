<?php
namespace Common\Security;

/**
 * Interface to define ACL rules transport object.
 *
 * @author Lukas Svoboda
 */
interface RuleMatrixInterface {

	/**
	 * Returns list of roles contained at this matrix.
	 *
	 * @return array
	 */
	public function getRoles();

	/**
	 * Returns list of resources contained at this matrix.
	 *
	 * @return array
	 */
	public function getResources();

	/**
	 * Returns true if role is allowed.
	 *
	 * @param string Role id
	 * @param string Resource name
	 * @return boolean
	 */
	public function isAllowed($roleId, $resourceName);

}