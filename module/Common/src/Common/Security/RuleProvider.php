<?php
namespace Common\Security;

use Zend\I18n\Translator\LoaderPluginManager;

use Common\Dao\RuleDao;

use Zend\ServiceManager\ServiceLocatorInterface;

use BjyAuthorize\Provider\Rule\ProviderInterface;

/**
 * Database rule provider.
 *
 * @author Lukas Svoboda
 */
final class RuleProvider implements ProviderInterface {

	private $rules;
	private $sl;

	public function __construct(array $options, ServiceLocatorInterface $serviceLocator) {
		if ($serviceLocator === null) {
			throw new \Exception('ServiceLocator cannot be null.');
		}
		$this->sl = $serviceLocator;
	}

	/**
	 * @param array of RuleMo
	 */
	private function loadRules($rules) {
		$allowRules = array();
		$dennyRules = array();
		foreach ($rules as $rule) {
			$array = array();
			$array[] = array($rule->roleId);
			$array[] = $rule->resourceId;
			$array[] = $rule->privilage;
			if ($rule->type === 'allow') {
				$allowRules[] = $array;
			} else if ($rule->type === 'denny') {
				$dennyRules[] = $array;
			} else {
				throw new \Exception("Wrong rule type.");
			}
		}
		$this->rules['allow'] = $allowRules;
		$this->rules['denny'] = $dennyRules;
	}

	/**
	 * (non-PHPdoc)
	 * @see \BjyAuthorize\Provider\Rule\ProviderInterface::getRules()
	 * @return array
	 */
	public function getRules() {
		if (!isset($this->rules)) {
			$this->loadRules($this->sl->get('Common\Dao\RuleDao')->fetch());
		}
		return $this->rules;
	}
}