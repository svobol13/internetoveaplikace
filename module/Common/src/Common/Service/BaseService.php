<?php

namespace Common\Service;

use Common\Mo\UserMo;

use Zend\Authentication\AuthenticationService;

use ZfcUser\Controller\Plugin\ZfcUserAuthentication;

use Zend\Db\Sql\Select;

use Common\Validator\Validators;

use Common\Hydrator\ModelObjectHydrator;

use Common\Dao\UserRoleDao;
use Common\Dao\VehicleDao;
use Common\Dao\UserDao;
use Common\Dao\TransferDao;
use Common\Dao\RouteDao;
use Common\Dao\RuleDao;
use Common\Dao\PlaceDao;
use Common\Dao\PartnerDao;
use Common\Dao\OrderDao;
use Common\Dao\FileDao;

use Common\Table\CustomerTable;
use Common\Table\UserRoleTable;
use Common\Table\VehicleTable;
use Common\Table\UserTable;
use Common\Table\TransferTable;
use Common\Table\RouteTable;
use Common\Table\RuleTable;
use Common\Table\PlaceTable;
use Common\Table\PartnerTable;
use Common\Table\OrderTable;
use Common\Table\FileTable;

use Zend\ServiceManager\ServiceManager;
use Zend\EventManager\StaticEventManager;
use Zend\EventManager\EventManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Common\Security\Event\SecurityEvent;

/**
 * Abstract class commonly used as parrent of a service class
 * especialy if ACL-like security is needed.
 *
 * @author Lukas Svoboda
 */
abstract class BaseService implements EventManagerAwareInterface, ServiceLocatorAwareInterface {

	public function __construct(ServiceLocatorInterface $sm) {
		$this->setServiceLocator($sm);
	}

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @var ServiceLocatorInterface
     */
    protected $services;

    /**
     * Method allowing aop-like design through __call php method
     * while calling secured child functions.
     *
     * @param string $method
     * @param mixed $args
     * @throws \Exception
     * @return mixed
     */
    public function __call($method, $args) {
        // Before method call action
        $this->before($method, $args);

        // Method call
        if (!method_exists($this, $method)) {
            throw new \Exception(sprintf("Method named \"%s\" does not exists.", $method));
        }
        // Open new transaction
        $dbAdapter = $this->getServiceLocator()->get("db_adapter");
        $connection = $dbAdapter->getDriver()->getConnection();
        try {
        	// start transaction
        	$connection->beginTransaction();
        	$result = call_user_func_array(array($this, $method), $args);
        	// commit transaction
        	$connection->commit();
        } catch (\Exception $e) {
        	// rollback transaction
        	$connection->rollback();
			throw $e;
        }

        // Place for possible after method call action
        // Return result if successfull
        return $result;
    }

    /**
     * @param string $method
     * @param mixed $args
     */
    private function before($method, $args) {
        $class = explode('\\', get_class($this));
        $class = end($class);

        $e = new SecurityEvent();
        $e->setName(SecurityEvent::EVENT_ACCESS);
        $e->setParams(array('class' => $class, 'method' => $method));
        $this->getEventManager()->trigger($e);
    }

    /**
     * Method for select preparation
     *
     * @param array $filterData Example
     * array(new Between('seats', 1, 4), 'brand' => 'Skoda', 'wife' => null);
     *
     * @param FilterParams|array $filterParams
     * @return Select $select
     */
    protected function prepareSelect($filterData = array(), $filterParams = array()) {
    	$select = new Select();

    	$select->where($filterData);

    	if (is_array($filterParams)) {
    		if (isset($filterParams['orderBy'])) {
    			$newOrderBy = array();
    			foreach ($filterParams['orderBy'] as $column => $order) {
    				$newOrderBy[StringUtils::camelCaseToUnderscoreCase($column)] = $order;
    			}
    			$select->order($newOrderBy);
    		}
    		if (isset($filterParams['limit'])) {
    			$select->limit($filterParams['limit']);
    		}
    		if (isset($filterParams['offset'])) {
    			$select->offset($filterParams['offset']);
    		}
    	} else if (is_object($filterParams)) {
    		if (isset($filterParams->orderBy)) {
    			$newOrderBy = array();
    			foreach ($filterParams->orderBy as $column => $order) {
    				$newOrderBy[StringUtils::camelCaseToUnderscoreCase($column)] = $order;
    			}
    			$select->order($newOrderBy);
    		}
    		if (isset($filterParams->limit)) {
    			$select->limit($filterParams->limit);
    		}
    		if (isset($filterParams->offset)) {
    			$select->offset($filterParams->offset);
    		}
    	}

    	return $select;
    }

    /**
     * @return AuthenticationService
     */
    private function getAuthenticationService() {
		return $this->getServiceLocator()->get('zfcuser_auth_service');
    }

    /**
     * Returns id of authenticated user.
     *
     * @return int $userId
     */
    protected function getAuthenticatedUserId() {
    	return $this->getAuthenticationService()->getIdentity()->getId();
    }

    /**
     * Returns authenticated user.
     * @return UserMo
     */
    protected function getAuthenticatedUser() {
    	// TODO nahovno zpusob to tahat dvakrat
		$userDao = $this->getServiceLocator()->get('Common\Dao\UserDao');
		return $userDao->fetchById($this->getAuthenticatedUserId());
    }

    //~ Initializers
    /**
     * @return CustomerDao
     */
    protected function getCustomerDao() {
        return $this->getServiceLocator()->get('Common\Dao\CustomerDao');
    }

    /**
     * @return FileDao
     */
    protected function getFileDao() {
        return $this->getServiceLocator()->get('Common\Dao\FileDao');
    }

    /**
     * @return OrderDao
     */
    protected function getOrderDao() {
        return $this->getServiceLocator()->get('Common\Dao\OrderDao');
    }

    /**
     * @return PartnerDao
     */
    protected function getPartnerDao() {
        return $this->getServiceLocator()->get('Common\Dao\PartnerDao');
    }

    /**
     * @return PlaceDao
     */
    protected function getPlaceDao() {
        return $this->getServiceLocator()->get('Common\Dao\PlaceDao');
    }

    /**
     * @return RuleDao
     */
    protected function getRuleDao() {
        return $this->getServiceLocator()->get('Common\Dao\RuleDao');
    }

    /**
     * @return RouteDao
     */
    protected function getRouteDao() {
        return $this->getServiceLocator()->get('Common\Dao\RouteDao');
    }

    /**
     * @return TransferDao
     */
    protected function getTransferDao() {
        return $this->getServiceLocator()->get('Common\Dao\TransferDao');
    }

    /**
     * @return UserDao
     */
    protected function getUserDao() {
        return $this->getServiceLocator()->get('Common\Dao\UserDao');
    }

    /**
     * @return UserRoleDao
     */
    protected function getUserRoleDao() {
        return $this->getServiceLocator()->get('Common\Dao\UserRoleDao');
    }

    /**
     * @return VehicleDao
     */
    protected function getVehicleDao() {
        return $this->getServiceLocator()->get('Common\Dao\VehicleDao');
    }

    //~ Table initializers

    /**
     * @return CustomerTable
     */
    protected function getCustomerTable() {
        return $this->getServiceLocator()->get('CustomerTable');
    }

    /**
     * @return FileTable
     */
    protected function getFileTable() {
        return $this->getServiceLocator()->get('FileTable');
    }

    /**
     * @return OrderTable
     */
    protected function getOrderTable() {
        return $this->getServiceLocator()->get('OrderTable');
    }

    /**
     * @return PartnerTable
     */
    protected function getPartnerTable() {
        return $this->getServiceLocator()->get('PartnerTable');
    }

    /**
     * @return PlaceTable
     */
    protected function getPlaceTable() {
        return $this->getServiceLocator()->get('PlaceTable');
    }

    /**
     * @return RouteTable
     */
    protected function getRouteTable() {
        return $this->getServiceLocator()->get('RouteTable');
    }

    /**
     * @return RuleTable
     */
    protected function getRuleTable() {
        return $this->getServiceLocator()->get('RuleTable');
    }

    /**
     * @return TransferTable
     */
    protected function getTransferTable() {
        return $this->getServiceLocator()->get('TransferTable');
    }

    /**
     * @return UserTable
     */
    protected function getUserTable() {
        return $this->getServiceLocator()->get('UserTable');
    }

    /**
     * @return UserRoleTable
     */
    protected function getUserRoleTable() {
        return $this->getServiceLocator()->get('UserRoleTable');
    }

    /**
     * @return VehicleTable
     */
    protected function getVehicleTable() {
        return $this->getServiceLocator()->get('VehicleTable');
    }

    //~ Hydrator initializers

    /**
     * @return ModelObjectHydrator
     */
    protected function getModelObjectHydrator() {
		return $this->getServiceLocator()->get('ModelObjectHydrator');
    }

    /**
     * @return Validators
     */
	protected function getValidators() {
		return $this->getServiceLocator()->get('Validators');
	}

    /**
     * @return \Zend\EventManager\EventManagerInterface
     */
    public function getEventManager() {
        return $this->eventManager;
    }

    public function setEventManager(EventManagerInterface $events) {
        $events->setIdentifiers(array(
            __CLASS__,
            get_called_class(),
        ));
        $this->eventManager = $events;
        return $this;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->services;
    }

}