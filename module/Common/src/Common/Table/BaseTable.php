<?php
namespace Common\Table;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Db\Adapter\Adapter;

use Zend\Db\TableGateway\TableGateway;

abstract class BaseTable extends TableGateway implements ServiceLocatorAwareInterface {

	private $tableName;
	private $sl;

	/**
	 *
	 * @param Adapter $adapter
	 */
	public function __construct(ServiceLocatorInterface $sm, $tableName = null) {
		parent::__construct(isset($tableName) ? $tableName : $this->tableName, $sm->get('db_adapter'));
	}

	/**
	 * @return ModelObjectHydrator
	 */
	protected function getModelObjectHydrator() {
        return $this->getServiceLocator()->get("ModelObjectHydrator");
	}

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->sl = $serviceLocator;
	}

	public function getServiceLocator() {
        return $this->sl;
	}
}