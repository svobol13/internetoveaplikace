<?php
namespace Common\Utils;

use Zend\Stdlib\ArrayUtils as Utils;

/**
 * Zend's ArrayUtils class extended by other usefull operations
 * with array's.
 *
 * @author Lukas Svoboda
 */
final class ArrayUtils extends Utils {

	/**
	 * By default converts array:
	 *
	 * array('entityId' => 'value', ... )
	 * to
	 * array('entity_id' => 'value', ... )
	 *
	 * or other direction if toCamelCase is true
	 * @param array $array By default underscore keyed array.
	 * @param boolean $toCamelCase
	 */
	public static function convertArrayKeys(array $array, $toCamelCase = false) {
		$new = array();
		foreach ($array as $key => $value) {
			if ($toCamelCase) {
				$new[StringUtils::underscoreCaseToCamelCase($key)] = $value;
			} else {
				$new[StringUtils::camelCaseToUnderscoreCase($key)] = $value;
			}
		}
		return $new;
	}
    
    public static function mergePostWithData($post , $files = array()) {
        
        $postWithFiles = $post;
        
        foreach ($files as $key => $fileArray) {
            $postWithFiles = array_merge(
                    $postWithFiles, array($key => $fileArray['tmp_name'])
            );
        }

        return $postWithFiles;
    }    

}