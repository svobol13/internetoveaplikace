<?php

namespace Common\Utils;

class FileUtils {
    
    public static $uploadFolder = "./public/uploads/";
    public static $uploadThumbFolder = "./public/uploads/thumbs/";
    public static $webFolder = "/uploads/";
    
    /**
     * 
     * @param type $imageNameFromSuperglobalFiles
     * @param string $control = none
     * @return string where new UploadedFileIsLocates
     */
    
    public static function moveUploadedFile($fileNameFromSuperglobalFiles, $fileType = 'none') {

        $source = $_FILES[$fileNameFromSuperglobalFiles]["tmp_name"];
        $name = $_FILES[$fileNameFromSuperglobalFiles]["name"];
        $target = '';

        if(!(strlen($name) > 1)){
            return;
        }
        
        $target = self::$uploadFolder . strtolower($name);
        $thumb_target =  self::$uploadThumbFolder . strtolower($name);
        $web_target = self::$webFolder . strtolower($name);
        
        // TODO nemá kontrola přijít jinam?
        if ($fileType == 'image') {
            if (!self::isFileImg($target)) {
                return '';
            }
            // file is moved at the method end, but here i need it to make thumb
            move_uploaded_file($source, $target);
            self::makeThumb($target, $thumb_target);
            return $target;
            
        }elseif($fileType == 'none'){
            
        }

        move_uploaded_file($source, $target);
        return $web_target;
    }
    
    public static function makeThumb($target, $thumb_target){
            $thumb = new SimpleImage();
            $thumb->load($target);
            $thumb->resizeToWidth(160);
            $thumb->save($thumb_target);        
    }

    public static function isFileImg($name) {
        // TODO replace with Zend Validator
        if (substr($name, -3) == "png"
                OR substr($name, -3) == "jpg"
                OR substr($name, -3) == "gif"
        ) {
            return true;
        }
        return false;
    }
}