<?php

namespace Common\Utils;

class LocaleUtils {
    
    public static function getLocale() {
        return self::getLng();
    }
    
    private static function thisURL() {
    	if (!isset($_SERVER["SERVER_NAME"])) {
        	return;
        }
        $pageURL = 'http';
        if (isset($_SERVER["HTTPS"]) AND $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    
    private static function getLng($lngs = array('cs', 'en', 'de', 'fr', 'jp', 'th', 'zh')){
        $url = self::thisURL();
        $urlParts = explode('/', $url);
        foreach($lngs as $lng){
            if (in_array($lng, $urlParts)) {
                // TODO more safety
                $_SESSION['lng'] = $lng;
                return $lng;
            }            
        }
        // default language
        $_SESSION['lng'] = 'en';
        return 'en';
    }
    
    
}