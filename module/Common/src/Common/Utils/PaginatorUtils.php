<?php

namespace Common\Utils;

use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

class PaginatorUtils {
    private static $instance;
    
    private function __construct() {
        // singleton
    }

    /**
     * @return PaginatorUtils
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Function returns paginator
     * 
     * @param Array $arrayWithData
     * @param Int $itemsPerPage
     * @param Int $actualPage
     * @return Paginator pagintor
     */
    public function getPaginator($arrayWithData, $actualPage = 1, $itemsPerPage = 20) {

        if ($arrayWithData === null || !($itemsPerPage >= 0) || !($actualPage >= 0)) {
            throw new Exception('Paginator cannot be create');
        }

        $paginator = new Paginator(new ArrayAdapter($arrayWithData));
        $paginator->setItemCountPerPage($itemsPerPage);
        $paginator->setCurrentPageNumber($actualPage);

        return $paginator;
    }

}

?>
