<?php
namespace Common\Utils;

/**
 *
 * @author Lukas Svoboda
 */
final class StringUtils {

	/**
	 * Function to convert CamelCaseString to underscore_case_string.
	 *
	 * @param string $string String to convert
	 * @return string
	 */
	public static function camelCaseToUnderscoreCase($string) {
		$string[0] = strtolower($string[0]);
		$func = create_function('$c', 'return "_" . strtolower($c[1]);');
		return preg_replace_callback('/([A-Z])/', $func, $string);
	}

	/**
	 * Function to convert an underscore_case_string to CamelCaseString.
	 *
	 * @param string $string String to convert
	 * @param boolean $capitalizeFirst First letter should be capital.
	 * @return string
	 */
	public static function underscoreCaseToCamelCase($string, $capitalizeFirst = false) {
		if($capitalizeFirst) {
			$string[0] = strtoupper($string[0]);
		}
		$func = create_function('$c', 'return strtoupper($c[1]);');
		return preg_replace_callback('/_([a-z])/', $func, $string);
	}

	/**
	 * Determines if the given string is blank or not.
	 * Behaviour is show at a table below.
	 *
	 * $string = null - true
	 * $string = "" - true
	 * $string = "  " - true
	 * $string = "frank" - false
	 * $string = "  frank  " - false
	 *
	 * @param string $string
	 * @return boolean $blank
	 */
	public static function isBlank($string) {
		// TODO implement
		throw new \Exception('Not implemented yet.');
	}

}