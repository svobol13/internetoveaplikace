<?php

namespace Common\Validator;

use Zend\InputFilter\InputFilter;

use Zend\InputFilter\Factory;

use Zend\InputFilter\InputFilterInterface;

use Zend\InputFilter\InputFilterAwareInterface;

use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Validator\AbstractValidator;

abstract class BaseValidator extends AbstractValidator {

	/**
	 * @var array $inputs
	 */
	protected $inputs = null;
	/**
	 * @var array $formInputs
	 */
	protected $formInputs = null;

	/**
	 * @var InputFilter
	 */
	protected $inputFilter;

	/**
	 * @var InputFilter
	 */
	protected $formInputFilter;

	/**
	 * @var ServiceLocatorInterface
	 */
	protected $serviceLocator;

	private $factory;

	/**
	 * @param value mixed|traversable
	 */
	public function isValid($value) {
		if ($value == null) {
			return false;
		}
		if (is_object($value)) {
			$value = get_object_vars($value);
		}
		$this->getInputFilter()->setData($value);
		return $this->getInputFilter()->isValid();
	}

	public function getMessages() {
		return $this->getInputFilter()->getMessages();
	}

	/**
	 * @return InputFilter
	 */
    public function getFormInputFilter() {
    	$filter = $this->getInputFilter();
    	// by default validate all
    	$filter->setValidationGroup(InputFilter::VALIDATE_ALL);
        return $this->getInputFilter();
    }

    /**
     * @return InputFilter
     */
    public abstract function getInputFilter();

    /**
     * @return \Zend\InputFilter\Factory
     */
    protected function getInputFactory() {
    	if (!isset($this->factory)) {
    		$this->factory = new Factory();
    	}
    	return $this->factory;
    }

    /**
     * @return array $inputs
     */
    protected function getInputs() {
    	if ($this->inputs == null) {
    		// lazy loading triggered
    		$this->inputs = array();
    		$this->getInputFilter();
    	}
    	return $this->inputs;
    }

    /**
     * To avoid stackoverflow do not call this method within
     * BaseValidator::getFormInputFilter() implementation.
     *
     * @return array $formInputs
     */
    protected function getFormInputs() {
    	if ($this->formInputs == null) {
    		// lazy loading triggered
    		$this->formInputs = array();
    		$this->getFormInputFilter();
    	}
    	return $this->formInputs;
    }
}
