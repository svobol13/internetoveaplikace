<?php

namespace Common\Validator\FieldValidator;

use Zend\Validator\AbstractValidator;

class DateBeforeDateValidator extends AbstractValidator {
        
    const NOT_VALID = 'validation.datebeforedate.notValid';
    
    protected $messageTemplates = array(
        self::NOT_VALID => "One date have to be before the second one",
    );
    
    private $fieldToCompareWith;
    
    public function __construct($fieldToCompareWith, $options = null) {
    	if (!isset($fieldToCompareWith)) {
    		throw new \Exception('FieldToCompareWith must be set.');
    	}
        parent::__construct($options);
        $this->fieldToCompareWith = $fieldToCompareWith;
    }
    
    public function isValid($value, $context = null) {
    	$date = $context[$this->fieldToCompareWith];
    	if (!isset($date)) {
    		throw new \Exception(sprintf("Field %s is not known at the given context.", $this->fieldToCompareWith));
    	}
    	
        if (strtotime($value) < strtotime($date)) {
            $this->error(self::NOT_VALID);
        }
        
        if (count($this->getMessages())) {
            return false;
        } else {
            return true;
        }
    }
}