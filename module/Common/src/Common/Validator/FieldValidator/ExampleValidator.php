<?php

namespace Common\Validator\FieldValidator;

use Zend\Validator\AbstractValidator;

use Zend\Validator\ValidatorInterface;

class ExampleValidator extends AbstractValidator {

	const NOT_VALID = 'validation.custom.example.notValid';
	
    protected $messageTemplates = array(
        self::NOT_VALID   => "Invalid inpu. Value must be 'example'.",
    );

	public function isValid($value, $context = null) {
		// Valid only if value is 'example'
		$result = true;
		if ($value != 'example') {
			$this->error(self::NOT_VALID);
		}
		// More validation can be here

		if (count($this->getMessages())) {
			return false;
		} else {
			return true;
		}
	}
}