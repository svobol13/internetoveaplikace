<?php

namespace Common\Validator\FieldValidator;

use Zend\Validator\AbstractValidator;

class MaxSeatsInCar extends AbstractValidator {

    const NOT_VALID = 'validation.maxseats.notValid';

    protected $messageTemplates = array(
        self::NOT_VALID => "Availible seats is too big",
    );
    private $maxSeats;

    public function __construct($maxSeats, $options = null) {
        if (!isset($maxSeats)) {
            throw new \Exception('Max seats have to be set.');
        }
        parent::__construct($options);
        $this->maxSeats = $maxSeats;
    }

    public function isValid($value, $context = null) {
        $maxSeats = $context[$this->maxSeats];

        if (!isset($maxSeats)) {
            $maxSeats = null;
        }

        // Car is optional then maxSeats is null
        if ((($value > $maxSeats) || (!($value >= 1 ))) AND $maxSeats != null) {
            $this->error(self::NOT_VALID);
        }

        if (count($this->getMessages()) > 0) {
            return false;
        } else {
            return true;
        }
    }

}