<?php

namespace Common\Validator;

use Zend\ServiceManager\ServiceLocatorInterface;
use Common\Validator\CustomerValidator;
use Common\Validator\OrderValidator;
use Common\Validator\PartnerValidator;
use Common\Validator\PlaceValidator;
use Common\Validator\RoleValidator;
use Common\Validator\TransferValidator;
use Common\Validator\UserValidator;
use Common\Validator\VehicleValidator;

/**
 * Source class for all validators.
 *
 * @author Lukas Svoboda
 */
final class Validators {

    private $sm;

    public function __construct(ServiceLocatorInterface $sm) {
        $this->sm = $sm;
    }

}