angular.module('intapp', []).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
            when('/index', {templateUrl: 'home', controller: IndexCtrl}).
            when('/chat', {templateUrl: 'chat', controller: ChatController}).
            when('/:page', {templateUrl: 'pages', controller: PageCtrl}).
            otherwise({redirectTo: '/index'});
    }]);