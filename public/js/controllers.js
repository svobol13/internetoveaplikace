var updateFunction;
function ChatController($scope, $http) {

    $scope.message = new Message();

    updateFunction = function() {
        $http.get('rest/chat/messages/').success(function(data) {
            $scope.messages = data;
        });
    }

    // $scope.messages = [ {
    // "sender" : "Franta",
    // "message" : "Cau",
    // "time" : "2013-04-16 14:50:50"
    // }, {
    // "sender" : "Pepa",
    // "message" : "Nazdar",
    // "time" : "2013-04-16 14:55:50"
    // }, ];

    $scope.orderProp = 'time';

    $scope.sendMessage = function() {
        var date;
        date = new Date();
        date = date.getUTCFullYear() + '-'
                + ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-'
                + ('00' + date.getUTCDate()).slice(-2) + ' '
                + ('00' + date.getUTCHours()).slice(-2) + ':'
                + ('00' + date.getUTCMinutes()).slice(-2) + ':'
                + ('00' + date.getUTCSeconds()).slice(-2);
        $scope.message.time = date;
        $http.post("rest/chat", $scope.message).success(
                $scope.messages.push($scope.message));
        $scope.message = new Message($scope.message.sender);
    };

    startUpdate();
//    var timer = setInterval(function() {
//        $scope.update();
//    }, 3000);

}

function IndexCtrl($scope, $http) {
    $http.get('rest/resources/pages/').success(function(data) {
        $scope.pages = data;
    });
    
    $http.get('rest/resources/settings').success(function(data) {
        if (data.length === 0) return;
        $scope.settings = {};
        $scope.settings.color = data.color;
        $scope.settings.displayForm = data.displayForm;
    });
    
    $scope.colors = [
        {label: 'modra', value: '#3F5FBF'},
        {label: 'cervena', value: '#BF0006'}
    ];

    $scope.submitRules = function() {
        $http.put('rest/resources/settings', $scope.settings, {withCredentials: true}).success(function(data) {
            
        });
    };
}

function PageCtrl($scope, $http, $routeParams) {
    $http.get('rest/resources/' + $routeParams.page).success(function(data) {
        $scope.content = data[0];
        setPageActive($routeParams.page);
        if ($routeParams.page !== 'chat') {
            stopUpdate();
        }
    });
}

function Message(sender) {
    this.sender = sender;
    this.time = "";
    this.message = "";
}


//~ Ugly functions

function setPageActive($page) {
    // TODO
//    jQuery("#li_id_*").each(function() {
//        alert(this);
//    });
//    alert($page);
}

var timer;
function startUpdate() {
    timer = setInterval(function() {
        updateFunction();
    }, 3000);
}

function stopUpdate() {
    clearInterval(timer);
}