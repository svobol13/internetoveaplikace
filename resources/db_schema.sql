DROP TABLE IF EXISTS messages;
CREATE TABLE IF NOT EXISTS messages (
	id int(11) unsigned NOT NULL UNIQUE auto_increment,
	time timestamp not null,
	sender varchar(45) not null,
	message varchar(10000) not null,
	PRIMARY KEY(id)
);

DROP TABLE IF EXISTS pages;
CREATE TABLE IF NOT EXISTS pages (
	name varchar(45) not null,
	content text not null,
	PRIMARY KEY (name)
);

INSERT INTO pages (name, content) VALUES ('about', 'ABOUTUM ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO pages (name, content) VALUES ('contact', 'CONTACTUM ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
INSERT INTO pages (name, content) VALUES ('reference', 'REFERENCUM ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');

